<!-- <script setup>
import {PButton} from "../../../node_modules/@pirate-vite-ui/components";
</script> -->

## Button 按钮

<p-button>默认按钮</p-button>
<p-button type="primary">默认按钮</p-button>

::: details 显示代码

```html
<p-button>默认按钮</p-button> <p-button type="primary">默认按钮</p-button>
```

:::
