import DefaultTheme from "vitepress/theme";
import pirate from "@pirate-vite-ui/components";
export default {
  ...DefaultTheme,
  enhanceApp: async ({ app }) => {
    // app is the Vue 3 app instance from `createApp()`. router is VitePress'
    // custom router. `siteData`` is a `ref`` of current site-level metadata.
    app.use(pirate);
  },
};