import { createApp } from "vue";
import App from "./app.vue";
import pirate from "@pirate-vite-ui/components";
const app = createApp(App);
app.use(pirate);
app.mount("#app");

